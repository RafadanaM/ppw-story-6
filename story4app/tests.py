from django.test import TestCase
from django.test.client import Client
from .models import StatusModel
from .forms import StatusForm
from .views import index
from django.urls import resolve

# Create your tests here
class Story6TestCase(TestCase):

    def testUrl(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)


    def testModel(self):
        w = StatusModel.objects.create(status = "Test")

        self.assertTrue(isinstance(w, StatusModel))
        self.assertEqual(w.__str__(), w.status)
        counter = StatusModel.objects.all().count()
        self.assertEqual(counter,1)

     

    def test_form_add_status(self):
        form = StatusForm({'status' : 'Test'})
        self.assertTrue(form.is_valid())

    def test_valid_post(self):
        response = self.client.post('', {'status' : 'test'}, follow = True)
        html_response = response.content.decode('utf8')
        self.assertIn('test', html_response)

    def test_invalid_post(self):
        form = StatusForm({'status' : ''})
        self.assertFalse(form.is_valid())
    
    