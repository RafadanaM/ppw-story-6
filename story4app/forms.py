from django import forms
from .models import StatusModel

class StatusForm(forms.ModelForm):
    class Meta:
        model = StatusModel
        fields = ('status',)
        widgets = {'status' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'TEST'})}

