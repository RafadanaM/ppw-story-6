from django.shortcuts import render
from .models import StatusModel
from .forms import StatusForm
# Create your views here.
def index(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()

    form = StatusForm()
    statusList = StatusModel.objects.all()

    response = {'form' : form, 'statusList' : statusList}
    
    return render(request, 'index.html', response)




